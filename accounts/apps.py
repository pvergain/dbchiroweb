from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'accounts'
    label = 'Gestion des comptes observateurs'
