from django.contrib import admin
from .models import TypeDevice, TypePlace, Contact, BiomDent, PlaceManagementAction, LandCoverCLC, Specie, SpecieStatus, SpecieDetail

admin.site.register(TypePlace)
admin.site.register(Contact)
admin.site.register(TypeDevice)
admin.site.register(BiomDent)
admin.site.register(PlaceManagementAction)
admin.site.register(LandCoverCLC)
admin.site.register(Specie)
admin.site.register(SpecieDetail)
admin.site.register(SpecieStatus)
