from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin

from .models import Territory, Municipality, LandCover

admin.site.register(Territory, LeafletGeoAdmin)
admin.site.register(Municipality, LeafletGeoAdmin)
admin.site.register(LandCover, LeafletGeoAdmin)
