from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView


class UnauthorizedModify(LoginRequiredMixin, TemplateView):
    template_name = 'unauthorized.html'

    def get_context_data(self, **kwargs):
        context = super(UnauthorizedModify, self).get_context_data(**kwargs)
        context['message'] = 'Vous n\'êtes pas autorisé à modifier ou supprimer cette donnée'
        return context


class UnauthorizedView(LoginRequiredMixin, TemplateView):
    template_name = 'unauthorized.html'

    def get_context_data(self, **kwargs):
        context = super(UnauthorizedView, self).get_context_data(**kwargs)
        context['message'] = 'Vous n\'êtes pas autorisé à voir cette donnée'
        return context
